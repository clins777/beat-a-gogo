package br.ufpe.cin.beatagogo.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import br.ufpe.cin.beatagogo.Constant;
import br.ufpe.cin.beatagogo.R;
import br.ufpe.cin.beatagogo.SongHolder;

/**
 * Created by joao_ on 20/06/2017.
 */

public class MainMenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main_menu);
        View mContentView = findViewById(R.id.main_menu);

        mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

        Constant.ATTEMPTS = 0;

        final Button mRecordChallengeButton = (Button) findViewById(R.id.record_challenge_btn);
        final Button mAcceptChallengeButton = (Button) findViewById(R.id.accept_challenge_btn);

        mRecordChallengeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainMenuActivity.this, PlayChallengeActivity.class);
                SongHolder.getSingleton().setActivityClass(ConfirmBeatActivity.class);
                startActivity(intent);
                finish();
            }
        });

        mAcceptChallengeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainMenuActivity.this, ListenChallengeActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
