package br.ufpe.cin.beatagogo.models;

/**
 * Created by joao_ on 22/06/2017.
 */

public class Beat {
    private Note note;
    private float duration;
    private float startAt;
    private long timestamp;

    public Beat() {

    }

    public Beat(Note note, long timestamp) {
        this.note = note;
        this.timestamp = timestamp;
    }

    public Beat(Note note, float duration, float startAt) {
        this.note = note;
        this.duration = duration;
        this.startAt = startAt;
    }

    public void fillStartAt(long start) {
        this.startAt = 1f * (timestamp - start) / 1000f;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }


    public Note getNote() {
        return note;
    }

    public void setNote(Note note) {
        this.note = note;
    }

    public float getDuration() {
        return duration;
    }

    public void setDuration(float duration) {
        this.duration = duration;
    }

    public void setStartAt(float startAt) {
        this.startAt = startAt;
    }

    public float getStartAt() {
        return startAt;
    }

    @Override
    public String toString() {
        return "Beat{" +
                "note=" + note +
                ", duration=" + duration +
                ", startAt=" + startAt +
                ", timestamp=" + timestamp +
                '}';
    }
}
