package br.ufpe.cin.beatagogo.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import br.ufpe.cin.beatagogo.Constant;
import br.ufpe.cin.beatagogo.R;

/**
 * Created by joao_ on 21/06/2017.
 */

public class RecordActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_record);
        View mContentView = findViewById(R.id.record);

        mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

        new CountDownTimer(5000, 1000) {

            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                String flag = "playing";
                boolean error = false;

                try {
                    Bundle bundle = getIntent().getExtras();
                    flag = bundle.getString("flag");
                } catch (Exception e) {
                    Log.d("MISS BUNDLE PARAMETER", "Parâmetro flag não encontrado.");
                } finally {
                    Intent intent = null;
                    if (!error) {
                        if (flag.equals("playing"))
                            intent = new Intent(RecordActivity.this, ResultActivity.class);
                        else
                            intent = new Intent(RecordActivity.this, SendBeatActivity.class);
                    } else {
                        Constant.ATTEMPTS++;
                        intent = new Intent(RecordActivity.this, BeatBadFormatActivity.class);
                    }
                    startActivity(intent);
                    finish();
                }
            }
        }.start();
    }
}
