package br.ufpe.cin.beatagogo.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import br.ufpe.cin.beatagogo.R;

public class SplashyActivity extends AppCompatActivity {

    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splashy);
        View mContentView = findViewById(R.id.splash);

        mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

        final Activity context = this;

        Runnable autoHide = new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(context, CalibrationActivity.class);
                startActivity(intent);
                context.finish();
            }
        };

        Handler handler = new Handler();
        handler.postDelayed(autoHide, AUTO_HIDE_DELAY_MILLIS);
    }
}
