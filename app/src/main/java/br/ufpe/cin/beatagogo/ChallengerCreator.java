package br.ufpe.cin.beatagogo;

import br.ufpe.cin.beatagogo.models.Song;

public class ChallengerCreator {

    private Song currentSong;

    private static ChallengerCreator instance;

    public static ChallengerCreator getSingleton() {
        if (instance == null)
            instance = new ChallengerCreator();
        return instance;
    }

    public Song getCurrentSong() {
        return currentSong;
    }

    public void setCurrentSong(Song currentSong) {
        this.currentSong = currentSong;
    }
}
