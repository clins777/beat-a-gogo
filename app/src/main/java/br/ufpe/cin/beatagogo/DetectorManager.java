package br.ufpe.cin.beatagogo;

import android.content.Context;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.util.Log;

import java.util.LinkedList;

/**
 * Created by rinsukayu on 28/06/17.
 */

public class DetectorManager {

    private Thread streamThread;
    private Classifier classifier;
    private AudioRecord recorder;
    private int sampleRate = 16000;
    private int channelConfig = AudioFormat.CHANNEL_IN_MONO;
    private int audioFormat = AudioFormat.ENCODING_PCM_16BIT;
    private int minBufSize = AudioRecord.getMinBufferSize(sampleRate, channelConfig, audioFormat);
    private boolean status = true;
    private float[] data;
    private int bufferSize = 10;
    private int numSplits = 3;
    private float bufferAvgEnergy;
    private float bufferVariance;
    private LinkedList<Float> buffer = new LinkedList<>();
    private LinkedList<float[]> bufferSamples = new LinkedList<>();
    private int slot_size = 1024;
    private int countDetectFromLastDetect = 10000;

    private LinkedList<Integer> tuns = new LinkedList<>();
    private LinkedList<Integer> pas = new LinkedList<>();

    private static DetectorManager instance;

    public static DetectorManager getSingleton() {
        if (instance == null)
            instance = new DetectorManager();
        return instance;
    }

    public void interruptThread() {
        streamThread = null;
    }

    public void killClassifier() {
        synchronized (getSingleton()) {
            classifier = null;
        }
    }

    public void setLists(LinkedList<Integer> tuns, LinkedList<Integer> pas) {
        this.tuns = tuns;
        this.pas = pas;
    }

    public void startThread(ITumPaDetector iTumPa, Context context) {
        synchronized (getSingleton()) {
            Log.d("DEBUG", "CREATING ITUMPA CLASSIFIER");
            classifier = new Classifier(tuns, pas, iTumPa);
        }
        if (streamThread == null)
            startStreaming(context);
    }

    public void startThread(ICalibrator iCalib, Context context) {
        synchronized (getSingleton()) {
            classifier = new Classifier(iCalib);
        }
        if (streamThread == null)
            startStreaming(context);
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    private float getSignalEnergy(float[] samples1) {
        float energy = 0.0f;
        for (int index = 0; index < samples1.length; index++)
            energy = energy + (samples1[index] * samples1[index]);
        return energy;
    }

    public AudioRecord findAudioRecord(Context context) {
        AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        int _rate = Integer.parseInt(audioManager.getProperty(AudioManager.PROPERTY_OUTPUT_SAMPLE_RATE));
        //for (int rate : mSampleRates) {
        for (int audioFormat : new int[]{AudioFormat.ENCODING_PCM_FLOAT, AudioFormat.ENCODING_PCM_8BIT, AudioFormat.ENCODING_PCM_16BIT}) {
            for (int channelConfig : new int[]{AudioFormat.CHANNEL_IN_MONO, AudioFormat.CHANNEL_IN_STEREO}) {
                try {
                    // Log.d(C.TAG, "Attempting rate " + rate + "Hz, bits: " + audioFormat + ", channel: "
                    //         + channelConfig);
                    int minBSize = AudioRecord.getMinBufferSize(_rate, channelConfig, audioFormat);
                    if (minBSize != AudioRecord.ERROR_BAD_VALUE) {
                        // check if we can instantiate and have a success
                        AudioRecord recorder = new AudioRecord.Builder()
                                .setAudioSource(MediaRecorder.AudioSource.DEFAULT)
                                .setAudioFormat(new AudioFormat.Builder()
                                        .setEncoding(audioFormat)
                                        .setSampleRate(_rate)
                                        .setChannelMask(channelConfig)
                                        .build()).setBufferSizeInBytes(minBSize).build();
//                            AudioRecord recorder = new AudioRecord(MediaRecorder.AudioSource.MIC, rate, channelConfig, audioFormat, bufferSize);


                        if (recorder.getState() == AudioRecord.STATE_INITIALIZED) {
                            return recorder;
                        }
                    }
                } catch (Exception e) {
                    //                   System.out.print("Exception, keep trying.");
                }
            }
        }
        //}
        return null;
    }

    private synchronized void computeBufferAvgEnergyAndVariance() {

        bufferAvgEnergy = 0.0f;
        for (int i = 0; i < buffer.size(); i++) {
            bufferAvgEnergy += buffer.get(i).floatValue();
        }
        bufferAvgEnergy /= buffer.size();

        bufferVariance = 0.0f;
        for (int k = 0; k < buffer.size(); k++) {
            bufferVariance = bufferVariance +
                    (buffer.get(k).floatValue() - bufferAvgEnergy) * (buffer.get(k).floatValue() - bufferAvgEnergy);
        }
        bufferVariance = bufferVariance / buffer.size();
    }

    private synchronized float getCenterEnergy() {
        int pos = buffer.size() / 2;
        return buffer.get(pos);
    }

    private float[] getCenterSample() {
        int pos = bufferSamples.size() / 2;
        return bufferSamples.get(pos);
    }

    private synchronized void updateBuffer(float new_value, float[] sample) {
        buffer.add(new Float(new_value));
        buffer.remove(buffer.get(0));

        bufferSamples.add(sample);
        bufferSamples.remove(bufferSamples.get(0));
    }

    private synchronized void insertInBuffer(float value, float[] sample) {
        buffer.add(new Float(value));
        bufferSamples.add(sample);
    }

    private float[] detectBeat(float[] sample) {
        float sampleEnergy = getSignalEnergy(sample);
        computeBufferAvgEnergyAndVariance();
        float W = -0.0025714f * bufferVariance + 1.5142857f;
        updateBuffer(sampleEnergy, sample);
        countDetectFromLastDetect++;
        if (getCenterEnergy() > 2 * W * bufferAvgEnergy && countDetectFromLastDetect > 1) {
            float[] s = getCenterSample();
            countDetectFromLastDetect = 0;
            return s;
        }
        return null;
    }

    public void startStreaming(Context context) {
        //data = new float[minBufSize];
        data = new float[slot_size];

        //recorder = new AudioRecord(MediaRecorder.AudioSource.MIC,sampleRate,channelConfig,audioFormat,minBufSize*10);
        recorder = findAudioRecord(context);
        //new AudioRecord(MediaRecorder.AudioSource.MIC,8000,channelConfig,audioFormat,slot_size*10);

        recorder.startRecording();
        // Inicializando buffer
        for (int i = 0; i < bufferSize / numSplits; i++) {
            minBufSize = recorder.read(data, 0, data.length, AudioRecord.READ_BLOCKING);
            //float[] sample = ByteBuffer.wrap(data).getFloat();
            LinkedList<float[]> l = splitSample(data, numSplits);
            for (float[] s : l) {
                float energy = getSignalEnergy(s);
                insertInBuffer(energy, s);
            }
        }

        streamThread = new Thread(new Runnable() {
            public void run() {
                while (status == true) {
                    synchronized (getSingleton()) {
                        minBufSize = recorder.read(data, 0, data.length, AudioRecord.READ_BLOCKING);
                        LinkedList<float[]> l = splitSample(data, numSplits);
                        for (float[] s : l) {
                            //float[] sample = ByteBuffer.wrap(data).getFloat();
                            float[] detSample = detectBeat(s);
                            if (detSample != null && classifier != null)
                                classifier.onDetect(s, 0.0f, getSignalEnergy(s));
                        }
                    }
                }
            }
        });

        streamThread.start();
    }

    private LinkedList<float[]> splitSample(float[] data, int num) {
        LinkedList<float[]> l = new LinkedList<float[]>();
        for (int i = 0; i < num; i++) {
            float[] s = new float[data.length / num];
            for (int j = 0; j < s.length; j++) {
                s[j] = data[j + s.length * i];
            }
            l.add(s);
        }
        return l;
    }


}
