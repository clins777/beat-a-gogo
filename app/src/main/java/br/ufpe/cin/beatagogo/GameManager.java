package br.ufpe.cin.beatagogo;

import android.content.Intent;

import br.ufpe.cin.beatagogo.models.Song;

public class GameManager {

    private Song currentSong;

    private Intent afterSongIntent;

    private static GameManager instance;

    public static GameManager getSingleton() {
        if (instance == null)
            instance = new GameManager();
        return instance;
    }

    public Song getCurrentSong() {
        return currentSong;
    }

    public void setCurrentSong(Song currentSong) {
        this.currentSong = currentSong;
    }

    public void setIntent(Intent intent) {
        this.afterSongIntent = intent;
    }

    public Intent getIntent() {
        return this.afterSongIntent;
    }
}
