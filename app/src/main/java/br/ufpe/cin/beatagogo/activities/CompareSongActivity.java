package br.ufpe.cin.beatagogo.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import br.ufpe.cin.beatagogo.GameManager;
import br.ufpe.cin.beatagogo.R;
import br.ufpe.cin.beatagogo.SongHolder;
import br.ufpe.cin.beatagogo.SongMatch;

public class CompareSongActivity extends AppCompatActivity {

    public TextView compareMessage;

    public Button tentarDeNovoBtn;

    public Button novoDesafioBtn;

    public Button voltarBtn;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_compare_songs);
        View mContentView = findViewById(R.id.compare);

        mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);


        SongMatch sm = new SongMatch();
        float similarity = sm.getSongsSimilarity(SongHolder.getSingleton().getCurrentSong(), GameManager.getSingleton().getCurrentSong());

        compareMessage = (TextView) findViewById(R.id.compare_message);
        tentarDeNovoBtn = (Button) findViewById(R.id.tentar_de_novo);
        novoDesafioBtn = (Button) findViewById(R.id.novo_desafio);
        voltarBtn = (Button) findViewById(R.id.voltar);

        if (similarity > 0.4) {
            compareMessage.setText("Perfeito!");
            voltarBtn.setVisibility(View.INVISIBLE);
            tentarDeNovoBtn.setVisibility(View.INVISIBLE);
            novoDesafioBtn.setVisibility(View.VISIBLE);
        } else {
            compareMessage.setText("Errooou!");
            voltarBtn.setVisibility(View.VISIBLE);
            tentarDeNovoBtn.setVisibility(View.VISIBLE);
            novoDesafioBtn.setVisibility(View.INVISIBLE);
        }

        View.OnClickListener voltarBtnListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CompareSongActivity.this, MainMenuActivity.class);
                startActivity(intent);
                finish();
            }
        };

        voltarBtn.setOnClickListener(voltarBtnListener);
        novoDesafioBtn.setOnClickListener(voltarBtnListener);
        tentarDeNovoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CompareSongActivity.this, ListenChallengeActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }


}
