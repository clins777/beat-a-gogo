package br.ufpe.cin.beatagogo;

import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import org.billthefarmer.mididriver.MidiDriver;

import java.util.Collections;
import java.util.Comparator;

import br.ufpe.cin.beatagogo.models.Beat;
import br.ufpe.cin.beatagogo.models.Note;
import br.ufpe.cin.beatagogo.models.Song;

/**
 * Created by joao_ on 22/06/2017.
 */

public class MidiSynthesizer implements MidiDriver.OnMidiStartListener, View.OnTouchListener {

    private MidiDriver midiDriver;
    private byte[] event;
    private int[] config;

    public MidiSynthesizer() {
        midiDriver = new MidiDriver();
        midiDriver.setOnMidiStartListener(this);
        midiDriver.start();

        config = midiDriver.config();

        Log.d(this.getClass().getName(), "maxVoices: " + config[0]);
        Log.d(this.getClass().getName(), "numChannels: " + config[1]);
        Log.d(this.getClass().getName(), "sampleRate: " + config[2]);
        Log.d(this.getClass().getName(), "mixBufferSize: " + config[3]);
    }

    public void playNote(Note note, float duration) {
        // channel 1 = 153 (percussive)
        // channel 2 = 27-87
        // 31, 33, 35, 36, 37, 38, 40, 43, 46, 49, 51, 52 e 57

        if (note == Note.TUM) {
            // Construct a note ON message for the middle C at maximum velocity on channel 1:
            event = new byte[3];
            event[0] = (byte) 153; // 0x90 = note On, 0x00 = channel 1
            event[1] = (byte) 38; // 0x3C = middle C
            event[2] = (byte) 127; // 0x7F = the maximum velocity (127)
        } else if (note == Note.PA) {

            event = new byte[3];
            event[0] = (byte) 153;
            event[1] = (byte) 40;
            event[2] = (byte) 127;
        }

        // Send the MIDI event to the synthesizer.
        midiDriver.write(event);

        // Espera a duração da batida atual para seguir adiante
        try {
            Thread.sleep((long) (duration * 1000));
            // Stop note
            stopNote();
        } catch (InterruptedException ex) {
            Log.d("BEAT_DURATION", "Error in " + note);
        }
    }

    public void stopNote() {
        // Construct a note OFF message for the middle C at minimum velocity on channel 1:
        event = new byte[3];
        event[0] = (byte) 0x80;  // 0x80 = note Off, 0x00 = channel 1
        event[1] = (byte) 0x3C;  // 0x3C = middle C
        event[2] = (byte) 0x00;  // 0x00 = the minimum velocity (0)

        // Send the MIDI event to the synthesizer.
        midiDriver.write(event);
    }

    public void playSong(Song song) {
        // Ordena a lista de batidas pelo tempo no intervalo

        Collections.sort(song.getBeats(), new Comparator<Beat>() {
            @Override
            public int compare(Beat beat1, Beat beat2) {
                return beat1.getStartAt() > beat2.getStartAt() ? 1 : (beat1.getStartAt() < beat2.getStartAt()) ? -1 : 0;
            }
        });

        // Itera na lista de batidas do som
        for (int i = 0; i < song.getBeats().size(); i++) {
            Beat currentBeat = song.getBeats().get(i);
            playNote(currentBeat.getNote(), currentBeat.getDuration());
        }
    }

    public void start() {
        midiDriver.start();
    }

    public void stop() {
        midiDriver.stop();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return false;
    }

    @Override
    public void onMidiStart() {
        Log.d(this.getClass().getName(), "onMidiStart()");
    }
}