package br.ufpe.cin.beatagogo;

import br.ufpe.cin.beatagogo.models.Beat;
import br.ufpe.cin.beatagogo.models.Song;

public class SongMatch {

    float w_bias = 0.5f;
    float r_bias = 0.0f;
    float w_weight = 1.0f;
    float r_weight = 0.1f;

    float threshold = 0.5f;
    float tolerance = 0.1f;

    float get_beats_dissimilarity(Beat beat1, Beat beat2){
        if(beat1.getNote().getCode() == beat2.getNote().getCode())
            return r_bias + r_weight*(beat1.getStartAt() - beat2.getStartAt()) + r_weight*(beat1.getDuration() - beat2.getDuration());
        else
            return w_bias + w_weight*(beat1.getStartAt() - beat2.getStartAt()) + w_weight*(beat1.getDuration() - beat2.getDuration());
    }

    float get_beats_dissimilarity2(Beat beat1, Beat beat2, int songLength){
        if(beat1.getNote().getCode() == beat2.getNote().getCode())
            return 0.0f + 0.01f*(beat1.getStartAt() - beat2.getStartAt()) + 0.01f*(beat1.getDuration() - beat2.getDuration());
        else
            return 1.0f/songLength + 0.2f*(beat1.getStartAt() - beat2.getStartAt()) + 0.2f*(beat1.getDuration() - beat2.getDuration());
    }

    float get_beats_dissimilarity3(Beat beat1, Beat beat2, int songLength){
        float time = (float)Math.sqrt((beat1.getStartAt() - beat2.getStartAt())*(beat1.getStartAt() - beat2.getStartAt()));;
        float duration = (float)Math.sqrt((beat1.getDuration() - beat2.getDuration())*(beat1.getDuration() - beat2.getDuration()));;
        if(beat1.getNote().getCode() == beat2.getNote().getCode())
            return 0.0f + 0.01f*time + 0.01f*duration;
        else
            return 1.0f/songLength + 0.2f*time + 0.2f*duration;
    }

    float get_simple_dissimilarity(Beat beat1, Beat beat2){
        if (beat1.getNote().getCode() == beat2.getNote().getCode())
            return 0.0f;
        else
            return 1.0f;
    }

    float get_simple_dissimilarity2(Beat beat1, Beat beat2, int songLength){
        if (beat1.getNote().getCode() == beat2.getNote().getCode())
            return 0.0f;
        else
            return 1.0f/songLength;
    }

    float DTWD_distance(Song song1, Song song2) {
        int N = song1.getLength();
        int M = song2.getLength();

        float[][] DTW = new float[N][M];

        for(int i = 1; i < N; i++)
            DTW[i][0] = Float.MAX_VALUE;
        for(int i = 1; i < M; i++)
            DTW[0][i] = Float.MAX_VALUE;

        for(int i = 1; i < N; i++){
            for(int j = 1; j < M; j++) {
                float cost = get_beats_dissimilarity(song1.getBeats().get(i), song2.getBeats().get(j));
                //float cost = get_beats_dissimilarity2(song1.getBeats().get(i), song2.getBeats().get(j), Math.min(N,M));
                //float cost = get_beats_dissimilarity3(song1.getBeats().get(i), song2.getBeats().get(j), Math.min(N,M));
                //float cost = get_simple_dissimilarity(song1.getBeats().get(i), song2.getBeats().get(j));
                //float cost = get_simple_dissimilarity2(song1.getBeats().get(i), song2.getBeats().get(j), Math.min(N,M));
                DTW[i][j] = cost + Math.min(Math.min(DTW[i - 1][j], DTW[i][j - 1]), DTW[i - 1][j - 1]);
            }
        }

        return Math.abs(DTW[N - 1][M - 1]);
    }

    public boolean are_similar(Song song1, Song song2) {
        if(DTWD_distance(song1, song2) < (threshold - tolerance)) {
            return true;
        }
	    return false;
    }

    public float getSongsSimilarity(Song song1, Song song2) {
        return Math.max(0, 1 - threshold * DTWD_distance(song1, song2));
        // Testar sem threshold
        //return Math.max(0, 1 - DTWD_distance(song1, song2));
    }

}
