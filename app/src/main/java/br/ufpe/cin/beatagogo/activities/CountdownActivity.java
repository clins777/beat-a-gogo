package br.ufpe.cin.beatagogo.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import br.ufpe.cin.beatagogo.R;

/**
 * Created by joao_ on 20/06/2017.
 */

public class CountdownActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_countdown);
        View mContentView = findViewById(R.id.countdown);

        mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

        final ImageView mFirstImg = (ImageView) findViewById(R.id.count_3);
        final ImageView mSecondImg = (ImageView) findViewById(R.id.count_2);

        new CountDownTimer(4000, 1000) {

            public void onTick(long millisUntilFinished) {
                long current = millisUntilFinished / 1000;
                if (current == 2)
                    mFirstImg.setVisibility(View.GONE);
                if (current == 1)
                    mSecondImg.setVisibility(View.GONE);
            }

            public void onFinish() {
                String flag = "playing";

                try {
                    Bundle bundle = getIntent().getExtras();
                    flag = bundle.getString("flag");
                } catch (Exception e) {
                    Log.d("MISS BUNDLE PARAMETER", "Parâmetro flag não encontrado.");
                } finally {
                    Intent intent = new Intent(CountdownActivity.this, RecordActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("flag", flag);
                    intent.putExtras(bundle);
                    startActivity(intent);
                    finish();
                }
            }
        }.start();
    }
}
