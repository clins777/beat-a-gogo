package br.ufpe.cin.beatagogo.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.LinkedList;

import br.ufpe.cin.beatagogo.DetectorManager;
import br.ufpe.cin.beatagogo.ICalibrator;
import br.ufpe.cin.beatagogo.R;

public class CalibrationActivity extends AppCompatActivity implements ICalibrator {

    private boolean isCalibratedTum = false;
    private boolean isCalibratedPa = false;

    private boolean isCalibratingTum = false;
    private boolean isCalibratingPa = false;

    private LinkedList<Integer> tuns = new LinkedList<>();
    private LinkedList<Integer> pas = new LinkedList<>();

    @Override
    public void onDetect(int signalFeature) {
        if (isCalibratingTum) {
            Log.d("DEBUG", "Tum: " + signalFeature);
            tuns.add(signalFeature);
        }
        if (isCalibratingPa) {
            Log.d("DEBUG", "Pa: " + signalFeature);
            pas.add(signalFeature);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_calibration);
        View mContentView = findViewById(R.id.calibration);

        mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

        Typeface tfForMsg = Typeface.createFromAsset(getAssets(), "SoftLine.otf");

        final TextView mGuideMsg = (TextView) findViewById(R.id.calib_guide_msg);
        mGuideMsg.setTypeface(tfForMsg);

        final Button mOkBtn = (Button) findViewById(R.id.calib_ok_btn);
        mOkBtn.setTypeface(tfForMsg);

        Typeface tfForBtn = Typeface.createFromAsset(getAssets(), "olivier_demo.ttf");

        final Button mTumBtn = (Button) findViewById(R.id.tum_button);
        mTumBtn.setTypeface(tfForBtn);

        final Button mPaBtn = (Button) findViewById(R.id.pa_button);
        mPaBtn.setTypeface(tfForBtn);

        final Handler handler = new Handler();

        View.OnClickListener mCalibBtnOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTumBtn.setEnabled(false);
                mPaBtn.setEnabled(false);

                final Button btn = (Button) v;

                btn.setEnabled(false);

                if (btn.getText().length() == 3) {
                    isCalibratingTum = true;
                } else {
                    isCalibratingPa = true;
                }

                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (btn.getText().length() == 3) {
                            isCalibratingTum = false;
                            isCalibratedTum = true;
                            Toast.makeText(getApplicationContext(), "Tum calibrado!", Toast.LENGTH_SHORT).show();
                            mTumBtn.setEnabled(false);
                            mPaBtn.setEnabled(true);
                        } else {
                            isCalibratingPa = false;
                            isCalibratedPa = true;
                            Toast.makeText(getApplicationContext(), "Pa calibrado!", Toast.LENGTH_SHORT).show();
                            mPaBtn.setEnabled(false);
                            mTumBtn.setEnabled(true);
                        }

                        if (isCalibratedTum && isCalibratedPa) {
                            mGuideMsg.setVisibility(View.INVISIBLE);
                            mOkBtn.setVisibility(View.VISIBLE);
                            mTumBtn.setEnabled(false);
                            mPaBtn.setEnabled(false);
                        }
                    }
                }, 6000);

                ((TransitionDrawable) btn.getBackground()).startTransition(3000);
            }
        };

        mTumBtn.setOnClickListener(mCalibBtnOnClickListener);
        mPaBtn.setOnClickListener(mCalibBtnOnClickListener);
        mOkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOkBtn.setVisibility(View.INVISIBLE);
                mGuideMsg.setVisibility(View.VISIBLE);
                ((TransitionDrawable) mTumBtn.getBackground()).reverseTransition(500);
                ((TransitionDrawable) mPaBtn.getBackground()).reverseTransition(500);
                mTumBtn.setEnabled(true);
                mPaBtn.setEnabled(true);
                isCalibratedTum = false;
                isCalibratedPa = false;
                DetectorManager.getSingleton().setLists(tuns, pas);
                startActivity(new Intent(getApplicationContext(), MainMenuActivity.class));
            }
        });

        DetectorManager.getSingleton().startThread(this, getApplicationContext());
    }

    @Override
    public void onStop() {
        super.onStop();
        // Log.d("DEBUG", "Interrupting");
        // DetectorManager.getSingleton().interruptThread();
    }
}
