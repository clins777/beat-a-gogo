package br.ufpe.cin.beatagogo.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import br.ufpe.cin.beatagogo.R;
import br.ufpe.cin.beatagogo.models.Beat;
import br.ufpe.cin.beatagogo.models.Note;
import br.ufpe.cin.beatagogo.models.Song;

/**
 * Created by Caio Novaes on 26/06/17.
 */

public class FakeActivity extends Activity {

    private FirebaseDatabase database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fake);

        database = FirebaseDatabase.getInstance();

        // PEGA REFERENCIA PARA O DESAFIO NO FIREBASE
        final DatabaseReference myRef = database.getReference("song");

        Beat b1 = new Beat(Note.TUM, 1, 2);
        Beat b2 = new Beat(Note.PA, 1, 4);
        List<Beat> beats = new ArrayList<>();
        beats.add(b1);
        beats.add(b2);
        final Song dummy1 = new Song(beats);

        Beat b3 = new Beat(Note.TUM, 2, 0);
        Beat b4 = new Beat(Note.TUM, 2, 2);
        Beat b5 = new Beat(Note.PA, 2, 4);
        List<Beat> beats2 = new ArrayList<>();
        beats2.add(b3);
        beats2.add(b4);
        beats2.add(b5);
        final Song dummy2 = new Song(beats2);

        // PEGA DESAFIO NO FIREBASE CASO ELE MUDE, PARA PEGAR UMA VEZ USAR O addListenerForSingleValue
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String songString = dataSnapshot.getValue(Song.class).toString();
                Toast.makeText(getApplicationContext(), "Opa, mudou no bd: \n" + songString, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(getApplicationContext(), "Deu ruim lendo do firebase", Toast.LENGTH_SHORT).show();
            }
        });

        Button test1 = (Button) findViewById(R.id.test_btn_1);

        test1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // SALVA MUSICA NO FIREBASE
                myRef.setValue(dummy1);
            }
        });

        Button test2 = (Button) findViewById(R.id.test_btn_2);

        test2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // SALVA MUSICA NO FIREBASE
                myRef.setValue(dummy2);
            }
        });
    }

}
