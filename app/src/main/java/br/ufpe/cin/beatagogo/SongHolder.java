package br.ufpe.cin.beatagogo;

import android.content.Intent;

import br.ufpe.cin.beatagogo.models.Song;

/**
 * Created by Caio Novaes on 28/06/17.
 */

public class SongHolder {

    private Song currentSong;

    private Class afterSongActivityClass;

    private static SongHolder instance;

    public static SongHolder getSingleton() {
        if (instance == null)
            instance = new SongHolder();
        return instance;
    }

    public Song getCurrentSong() {
        return currentSong;
    }

    public void setCurrentSong(Song currentSong) {
        this.currentSong = currentSong;
    }

    public void setActivityClass(Class activityClass) {
        this.afterSongActivityClass = activityClass;
    }

    public Class getActivityClass() {
        return this.afterSongActivityClass;
    }
}
