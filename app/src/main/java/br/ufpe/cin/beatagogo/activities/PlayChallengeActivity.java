package br.ufpe.cin.beatagogo.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import br.ufpe.cin.beatagogo.DetectorManager;
import br.ufpe.cin.beatagogo.ITumPaDetector;
import br.ufpe.cin.beatagogo.MidiSynthesizer;
import br.ufpe.cin.beatagogo.R;
import br.ufpe.cin.beatagogo.SongHolder;
import br.ufpe.cin.beatagogo.models.Beat;
import br.ufpe.cin.beatagogo.models.Note;
import br.ufpe.cin.beatagogo.models.Song;

/**
 * Created by rinsukayu on 29/06/17.
 */

public class PlayChallengeActivity extends Activity implements ITumPaDetector {

    private long timestampStart;
    private long timestampEnd;
    private final MidiSynthesizer ms = new MidiSynthesizer();
    private final int bpm = 80;
    private final float pulseDuration = 60f / bpm;
    private int pulseCount = 5;
    private View mContentView;
    private Handler handler;

    private Thread thread = new Thread(new Runnable() {
        public void run() {
            ms.playSong(getMetronome());
        }
    });

    private Thread flickThread = new Thread(new Runnable() {
        public void run() {
            flicker();
        }
    });

    private List<Beat> beats = new ArrayList<>();

    @Override
    public void onTumDetection() {
        long timeMillis = new Date().getTime();
        Beat beat = new Beat(Note.TUM, timeMillis);
        beats.add(beat);
        Log.d("DEBUG", "TUM: " + beat.toString());
    }

    @Override
    public void onPaDetection() {
        long timeMillis = new Date().getTime();
        Beat beat = new Beat(Note.PA, timeMillis);
        beats.add(beat);
        Log.d("DEBUG", "PA: " + beat.toString());
    }

    private Song getMetronome() {
        List<Beat> beats = new ArrayList<>();
        beats.add(new Beat(Note.PA, pulseDuration, 0));
        beats.add(new Beat(Note.TUM, pulseDuration, pulseDuration));
        beats.add(new Beat(Note.TUM, pulseDuration, 2 * pulseDuration));
        beats.add(new Beat(Note.TUM, pulseDuration, 3 * pulseDuration));
        return new Song(beats);
    }

    private int pulseCount() {
        Log.d("DEBUG", "wow " + pulseCount);
        pulseCount = pulseCount - 1;
        return pulseCount;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DetectorManager.getSingleton().startThread(this, getApplicationContext());

        setContentView(R.layout.activity_play_challenge);

        mContentView = findViewById(R.id.countdown);

        mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

        Typeface tfForBtn = Typeface.createFromAsset(getAssets(), "olivier_demo.ttf");

        final TextView countdownText = (TextView) findViewById(R.id.countdown_text);
        countdownText.setTypeface(tfForBtn);

        final long durationMs = (long) Math.floor(6 * pulseDuration * 1000);
        final long pulseDurationMs = (long) Math.floor(pulseDuration * 1000);

        Log.d("DEBUG", "durationMs " + durationMs);
        Log.d("DEBUG", "pulseDurationMs " + pulseDurationMs);

        final CountDownTimer cdt = new CountDownTimer(durationMs, pulseDurationMs) {

            public void onTick(long millisUntilFinished) {
                int counter = pulseCount();
                Log.d("DEBUG", "common " + millisUntilFinished + " wow " + counter);
                if (counter == 4)
                    countdownText.setText("4...");
                if (counter == 3)
                    countdownText.setText("3...");
                if (counter == 2)
                    countdownText.setText("2...");
                if (counter == 1)
                    countdownText.setText("1...");
                if (counter == 0) {
                    thread.interrupt();
                    Log.d("DEBUG", "finished playing");
                    countdownText.setText("Gravando...");
                    timestampStart = new Date().getTime();
                    timestampEnd = timestampStart + (pulseDurationMs * 4);
                    Log.d("DEBUG", "inicio timestamp : " + timestampStart);
                    Log.d("DEBUG", "fim timestamp: " + timestampEnd);
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            countdownText.setText("Cabou...");
                            Toast.makeText(getApplicationContext(), "Gerando a sua solução!", Toast.LENGTH_SHORT).show();
                            List<Beat> newBeats = new ArrayList<>();
                            List<Beat> finalBeats = new ArrayList<>();

                            synchronized (DetectorManager.getSingleton()) {

                                for (Beat beat : beats) {
                                    if (beat.getTimestamp() >= timestampStart && beat.getTimestamp() <= timestampEnd) {
                                        beat.fillStartAt(timestampStart);
                                        newBeats.add(beat);
                                    }
                                }

                                for (int i = 0; i < newBeats.size(); i++) {
                                    Beat beat = newBeats.get(i);

                                    if (i == newBeats.size() - 1) {
                                        beat.setDuration(4 * pulseDuration - beat.getStartAt());
                                    } else {
                                        beat.setDuration(newBeats.get(i + 1).getStartAt() - beat.getStartAt());
                                    }

                                    finalBeats.add(beat);
                                }
                            }

                            Song song = new Song(finalBeats);

                            Log.d("DEBUG", song.toString());

                            Log.d("DEBUG", "KILL KILL KILL");

                            DetectorManager.getSingleton().killClassifier();

                            SongHolder.getSingleton().setCurrentSong(song);
                            Intent intent = new Intent(PlayChallengeActivity.this, SongHolder.getSingleton().getActivityClass());
                            startActivity(intent);
                        }
                    }, pulseDurationMs * 4);
                    // TODO: comecar a escutar
                }
            }

            public void onFinish() {

            }
        };

        handler = new Handler();

        Toast.makeText(getApplicationContext(), "Tocaremos o desafio em alguns instantes", Toast.LENGTH_SHORT).show();

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                thread.start();
                cdt.start();
            }
        }, 3000);
    }

    private void flicker() {
        final Drawable bg = mContentView.getBackground();
        mContentView.setBackgroundResource(R.color.orange);
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mContentView.setBackground(bg);
            }
        }, 200);
    }
}
