package br.ufpe.cin.beatagogo.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by joao_ on 22/06/2017.
 */

public class Song {
    private List<Beat> beats;

    public Song() {
        beats = new ArrayList<>();
    }

    public Song(List<Beat> beats) {
        this.beats = beats;
    }

    public List<Beat> getBeats() {
        return beats;
    }

    public void setBeats(List<Beat> beats) {
        this.beats = beats;
    }

    public int getLength() {
        return beats.size();
    }

    @Override
    public String toString() {
        String out = "Beats: \n";
        for (Beat beat : beats)
            out += beat.getNote().name() + " " + beat.getDuration() + " " + beat.getStartAt() + "\n";
        out += "Length: " + getLength() + "\n";
        return out;
    }
}