package br.ufpe.cin.beatagogo;

/**
 * Created by tuca_ on 24/06/2017.
 */

public interface IDetector {
    public void onDetect(float[] sample, float time, float energy);
}
