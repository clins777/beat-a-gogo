package br.ufpe.cin.beatagogo;

/**
 * Created by tuca_ on 28/06/2017.
 */

public interface ITumPaDetector {
    public void onTumDetection();
    public void onPaDetection();
}
