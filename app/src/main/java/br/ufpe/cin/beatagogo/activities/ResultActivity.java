package br.ufpe.cin.beatagogo.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import br.ufpe.cin.beatagogo.R;

/**
 * Created by joao_ on 20/06/2017.
 */

public class ResultActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_result);
        View mContentView = findViewById(R.id.result);

        mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

        final Button mNewChallengeButton = (Button) findViewById(R.id.new_challenge_btn);
        final TextView mChallengeSuccessTextView = (TextView) findViewById(R.id.challenge_success_msg);

        final Button mTryAgainButton = (Button) findViewById(R.id.try_again_btn);
        final TextView mTryAgainTextView = (TextView) findViewById(R.id.try_again_msg);
        final Button mBackButton = (Button) findViewById(R.id.back_btn);

        boolean success = false;

        try {
            Bundle bundle = getIntent().getExtras();
            success = bundle.getBoolean("success");
        } catch (Exception e) {
            Log.d("MISS BUNDLE PARAMETER", "Parâmetro success não encontrado.");
        } finally {
            if (!success) {
                mTryAgainButton.setVisibility(View.GONE);
                mTryAgainTextView.setVisibility(View.GONE);
                mBackButton.setVisibility(View.GONE);
                mNewChallengeButton.setVisibility(View.VISIBLE);
                mChallengeSuccessTextView.setVisibility(View.VISIBLE);
            }
        }

        mNewChallengeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ResultActivity.this, MainMenuActivity.class);
                startActivity(intent);
                finish();
            }
        });

        mTryAgainButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ResultActivity.this, CountdownActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("flag", "playing");
                intent.putExtras(bundle);
                startActivity(intent);
                finish();
            }
        });
    }
}
