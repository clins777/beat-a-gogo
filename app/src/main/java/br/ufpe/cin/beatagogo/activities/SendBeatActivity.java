package br.ufpe.cin.beatagogo.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import br.ufpe.cin.beatagogo.R;

/**
 * Created by joao_ on 20/06/2017.
 */

public class SendBeatActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_send_beat);
        View mContentView = findViewById(R.id.send_beat);

        mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

        final Button mConfirmButton = (Button) findViewById(R.id.confirm_beat_btn);
        final Button mSendButton = (Button) findViewById(R.id.send_beat_btn);
        final Button mBackButton = (Button) findViewById(R.id.back_btn);

        mConfirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mConfirmButton.setVisibility(View.GONE);
                mSendButton.setVisibility(View.VISIBLE);
            }
        });

        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SendBeatActivity.this, MainMenuActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
