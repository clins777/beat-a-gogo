package br.ufpe.cin.beatagogo.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import br.ufpe.cin.beatagogo.ChallengerCreator;
import br.ufpe.cin.beatagogo.R;
import br.ufpe.cin.beatagogo.SongHolder;
import br.ufpe.cin.beatagogo.SongMatch;

public class ResultCreationActivity extends AppCompatActivity {

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final FirebaseDatabase database = FirebaseDatabase.getInstance();

        final DatabaseReference root = database.getReference();

        setContentView(R.layout.activity_result_creation);
        View mContentView = findViewById(R.id.result);

        mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

        SongMatch sm = new SongMatch();

        float similarity = sm.getSongsSimilarity(SongHolder.getSingleton().getCurrentSong(), ChallengerCreator.getSingleton().getCurrentSong());

        Log.d("DEBUG", "SIMILIRADIDA: " + similarity);

        TextView textView = (TextView) findViewById(R.id.recording_result);

        if (similarity > 0.4) {
            textView.setText("Desafio cadastrado com sucesso");
            final DatabaseReference dbRef = root.child("challenges");
            dbRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    long count = dataSnapshot.getChildrenCount();
                    dbRef.child(Long.toString(count)).setValue(SongHolder.getSingleton().getCurrentSong());
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } else {
            textView.setText("Voce falhou");
        }

        final TextView mContinueBtn = (Button) findViewById(R.id.continue_btn);

        mContinueBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ResultCreationActivity.this, MainMenuActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
