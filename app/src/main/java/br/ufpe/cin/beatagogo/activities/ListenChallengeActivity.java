package br.ufpe.cin.beatagogo.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import br.ufpe.cin.beatagogo.GameManager;
import br.ufpe.cin.beatagogo.MidiSynthesizer;
import br.ufpe.cin.beatagogo.R;
import br.ufpe.cin.beatagogo.SongHolder;
import br.ufpe.cin.beatagogo.models.Song;

/**
 * Created by joao_ on 20/06/2017.
 */

public class ListenChallengeActivity extends AppCompatActivity {

    private Song currentChallenge;
    final MidiSynthesizer ms = new MidiSynthesizer();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FirebaseDatabase database = FirebaseDatabase.getInstance();

        DatabaseReference root = database.getReference();

        final DatabaseReference myRef = root.child("challenges");

        setContentView(R.layout.activity_listen_challenge);
        View mContentView = findViewById(R.id.listen_challenge);

        mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

        final Button mListenChallengeButton = (Button) findViewById(R.id.listen_challenge_btn);
        mListenChallengeButton.setEnabled(false);

        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<Song> songs = new ArrayList<>();

                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    songs.add(ds.getValue(Song.class));
                }

                Log.d("DEBUG", "song size: " + songs.size());

                if (songs.size() > 0) {
                    currentChallenge = songs.get(new Random().nextInt(songs.size()));
                    GameManager.getSingleton().setCurrentSong(currentChallenge);
                    mListenChallengeButton.setEnabled(true);
                } else {
                    Toast.makeText(getApplicationContext(), "Não há challenges... Grave uma!", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(getApplicationContext(), "Deu ruim no firebase", Toast.LENGTH_SHORT).show();
            }
        });

        final Button mListenAgainButton = (Button) findViewById(R.id.listen_again_btn);
        final Button mPlayChallengeButton = (Button) findViewById(R.id.play_challenge_btn);
        final Button mBackButton = (Button) findViewById(R.id.back_btn);
        final ImageView mListeningImg = (ImageView) findViewById(R.id.listening_img);

        mListenChallengeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListenChallengeButton.setVisibility(View.GONE);
                mBackButton.setEnabled(false);
                mListenAgainButton.setVisibility(View.VISIBLE);
                mListenAgainButton.setEnabled(false);
                mPlayChallengeButton.setVisibility(View.VISIBLE);
                mPlayChallengeButton.setEnabled(false);
                mListeningImg.setVisibility(View.VISIBLE);
                ms.playSong(currentChallenge);
                mListenAgainButton.setEnabled(true);
                mPlayChallengeButton.setEnabled(true);
                mBackButton.setEnabled(true);
            }
        });

        mListenAgainButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBackButton.setEnabled(false);
                mListenAgainButton.setEnabled(false);
                mPlayChallengeButton.setEnabled(false);
                ms.playSong(currentChallenge);
                mListenAgainButton.setEnabled(true);
                mPlayChallengeButton.setEnabled(true);
                mBackButton.setEnabled(true);
            }
        });

        mPlayChallengeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPlayChallengeButton.setEnabled(false);
                Intent intent = new Intent(ListenChallengeActivity.this, PlayChallengeActivity.class);
                SongHolder.getSingleton().setActivityClass(CompareSongActivity.class);
                startActivity(intent);
                finish();
            }
        });

        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ListenChallengeActivity.this, MainMenuActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}