package br.ufpe.cin.beatagogo;

import android.util.Log;

import java.util.Arrays;
import java.util.LinkedList;

/**
 * Created by tuca_ on 24/06/2017.
 */

public class Classifier implements IDetector {

    private LinkedList<Integer> tumCalibration;
    private LinkedList<Integer> paCalibration;

    private ICalibrator calibrator;
    private ITumPaDetector tumPaDetector;

    private int tum_counter;
    private int pa_counter;

    private float tum_tolerance;
    private float pa_tolerance;


    public Classifier(ICalibrator c) {
        this.calibrator = c;
    }

    public Classifier(LinkedList<Integer> tumSamples, LinkedList<Integer> paSamples, ITumPaDetector tumpa) {
        Log.d("DEBUG", " tuns: " + tumSamples.size() + " pas: " + paSamples.size());
        this.tumCalibration = tumSamples;
        this.paCalibration = paSamples;
        this.tumPaDetector = tumpa;

    }

    private int countSignalsChange(float[] sample) {
        int counter = 0;
        for (int i = 1; i < sample.length; i++) {
            if (sample[i] / sample[i - 1] < 0) {
                counter = counter + 1;
            }
        }
        return counter;
    }

    public void onDetect(float[] sample, float time, float energy) {

        int sampleCountValue = countSignalsChange(sample);
        Log.d("SampleCount --", "" + sampleCountValue);

        if (this.calibrator != null) {
            this.calibrator.onDetect(sampleCountValue);
        } else {
            String beat = knn(sampleCountValue);
            if (beat.equalsIgnoreCase("Pa")) {
                tumPaDetector.onPaDetection();
            } else {
                tumPaDetector.onTumDetection();
            }
        }

        /*if (sampleCountValue <= tum_counter + tum_tolerance && sampleCountValue >= tum_counter - tum_tolerance){
            System.out.print("TUM");
        }
        else if (sampleCountValue <= pa_counter + pa_tolerance && sampleCountValue >= pa_counter - pa_tolerance){
            System.out.print("PA");
        }
        else {
            System.out.print("Ruido");
        }*/
    }

    private String knn(int sampleFeature) {
        int k = 3;

        int[] tumDistance = new int[tumCalibration.size()];
        for (int i = 0; i < tumDistance.length; i++) {
            tumDistance[i] = Math.abs(sampleFeature - tumCalibration.get(i));
        }
        int[] paDistance = new int[paCalibration.size()];
        for (int i = 0; i < paDistance.length; i++) {
            paDistance[i] = Math.abs(sampleFeature - paCalibration.get(i));
        }
        Arrays.sort(paDistance);
        Arrays.sort(tumDistance);

        int tumIndex = 0;
        int paIndex = 0;

        boolean esgotou = false;

        while (tumIndex + paIndex < k && !esgotou) {
            if (tumIndex < tumCalibration.size()) {
                if (paIndex < paCalibration.size()) {
                    if (tumDistance[tumIndex] < paDistance[paIndex]) {
                        tumIndex++;
                    } else {
                        paIndex++;
                    }
                } else {
                    tumIndex++;
                }
            } else {
                if (paIndex < paCalibration.size()) {
                    paIndex++;
                } else {
                    esgotou = true;
                }
            }
        }

        if (tumIndex < paIndex) {
            return "Pa";
        } else {
            return "Tum";
        }

    }
}
