package br.ufpe.cin.beatagogo.models;

/**
 * Created by joao_ on 13/05/2017.
 */

public enum Note {
    TUM(1),
    PA(2);

    private int code;

    Note(int code) {
        this.code = code;
    }

    public int getCode() {
        return this.code;
    }
}
