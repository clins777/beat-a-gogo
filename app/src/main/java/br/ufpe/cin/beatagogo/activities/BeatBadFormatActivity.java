package br.ufpe.cin.beatagogo.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import br.ufpe.cin.beatagogo.Constant;
import br.ufpe.cin.beatagogo.R;

/**
 * Created by joao_ on 21/06/2017.
 */

public class BeatBadFormatActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_beat_bad_format);
        View mContentView = findViewById(R.id.beat_bad_format);

        mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

        final Button mTryAgainButton = (Button) findViewById(R.id.try_again_btn);
        final TextView mUnconfirmedBeatText = (TextView) findViewById(R.id.unconfirmed_beat_msg);

        final Button mTryAnotherBeatButton = (Button) findViewById(R.id.try_another_beat_btn);
        final TextView mInconsistentBeatText = (TextView) findViewById(R.id.inconsistent_beat_msg);
        final Button mBackButton = (Button) findViewById(R.id.back_btn);

        mTryAgainButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BeatBadFormatActivity.this, CountdownActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("flag", "recording");
                intent.putExtras(bundle);
                startActivity(intent);
                finish();
            }
        });

        mTryAnotherBeatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BeatBadFormatActivity.this, MainMenuActivity.class);
                startActivity(intent);
                finish();
            }
        });

        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BeatBadFormatActivity.this, MainMenuActivity.class);
                startActivity(intent);
                finish();
            }
        });

        try {
        } catch (Exception e) {
            Log.d("MISS BUNDLE PARAMETER", "Parâmetro error não encontrado.");
        } finally {
            // batida não confirmada
            if (Constant.ATTEMPTS < 3) {
                mTryAnotherBeatButton.setVisibility(View.GONE);
                mInconsistentBeatText.setVisibility(View.GONE);
                mTryAgainButton.setText("Tentar de novo (" + Constant.ATTEMPTS + "/3)");
                mTryAgainButton.setVisibility(View.VISIBLE);
                mUnconfirmedBeatText.setVisibility(View.VISIBLE);
                mBackButton.setVisibility(View.VISIBLE);
            } else { // batida inconsistente
                mTryAgainButton.setVisibility(View.GONE);
                mUnconfirmedBeatText.setVisibility(View.GONE);
                mBackButton.setVisibility(View.GONE);
                mTryAnotherBeatButton.setVisibility(View.VISIBLE);
                mInconsistentBeatText.setVisibility(View.VISIBLE);
            }
        }
    }
}