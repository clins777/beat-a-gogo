package br.ufpe.cin.beatagogo.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import br.ufpe.cin.beatagogo.R;

public class BeatActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_beat);
        View mContentView = findViewById(R.id.beat);

        mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

        Typeface tfForMsg = Typeface.createFromAsset(getAssets(), "SoftLine.otf");

        final TextView mNewCalibBtn = (Button) findViewById(R.id.new_calib_btn);
        mNewCalibBtn.setTypeface(tfForMsg);

        mNewCalibBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        final TextView mContinueBtn = (Button) findViewById(R.id.continue_btn);

        mContinueBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BeatActivity.this, MainMenuActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
