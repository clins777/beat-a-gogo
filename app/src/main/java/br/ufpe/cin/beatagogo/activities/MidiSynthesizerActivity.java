package br.ufpe.cin.beatagogo.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

import org.billthefarmer.mididriver.MidiDriver;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import br.ufpe.cin.beatagogo.R;
import br.ufpe.cin.beatagogo.models.Beat;
import br.ufpe.cin.beatagogo.models.Note;
import br.ufpe.cin.beatagogo.models.Song;

/**
 * Created by joao_ on 13/05/2017.
 */

public class MidiSynthesizerActivity extends AppCompatActivity implements MidiDriver.OnMidiStartListener, View.OnTouchListener {

    private MidiDriver midiDriver;
    private byte[] event;
    private int[] config;
    private Button tumButton;
    private Button paButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_midi_synthesizer);

        tumButton = (Button) findViewById(R.id.tumButton);
        tumButton.setOnTouchListener(this);

        paButton = (Button) findViewById(R.id.paButton);
        paButton.setOnTouchListener(this);

        midiDriver = new MidiDriver();
        midiDriver.setOnMidiStartListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        midiDriver.start();

        config = midiDriver.config();

        Log.d(this.getClass().getName(), "maxVoices: " + config[0]);
        Log.d(this.getClass().getName(), "numChannels: " + config[1]);
        Log.d(this.getClass().getName(), "sampleRate: " + config[2]);
        Log.d(this.getClass().getName(), "mixBufferSize: " + config[3]);

    }

    @Override
    protected void onPause() {
        super.onPause();
        midiDriver.stop();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        Log.d(this.getClass().getName(), "Motion event: " + event);

        if (v.getId() == R.id.tumButton) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                Log.d(this.getClass().getName(), "Tum Button MotionEvent.ACTION_DOWN");
//                playNote(Note.TUM);
                Song song = new Song();
                List<Beat> beats = new ArrayList<Beat>();
                beats.add(new Beat(Note.TUM, 1, 0));
                beats.add(new Beat(Note.TUM, 1, 2));
                beats.add(new Beat(Note.PA, 1, 4));
                song.setBeats(beats);
                playSong(song);
            }
//            if (event.getAction() == MotionEvent.ACTION_UP) {
//                Log.d(this.getClass().getName(), "Tum Button MotionEvent.ACTION_UP");
//                stopNote();
//            }
        }

        if (v.getId() == R.id.paButton) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                Log.d(this.getClass().getName(), "Pá Button MotionEvent.ACTION_DOWN");
//                playNote(Note.PA);
            }
            if (event.getAction() == MotionEvent.ACTION_UP) {
                Log.d(this.getClass().getName(), "Pá Button MotionEvent.ACTION_UP");
                stopNote();
            }
        }

        return false;
    }

    @Override
    public void onMidiStart() {
        Log.d(this.getClass().getName(), "onMidiStart()");
    }

    public void playSong(Song song) {
        // Ordena a lista de batidas pelo tempo no intervalo
        Collections.sort(song.getBeats(), new Comparator<Beat>() {
            @Override
            public int compare(Beat beat1, Beat beat2) {
                return beat1.getStartAt() > beat2.getStartAt() ? 1 : (beat1.getStartAt() < beat2.getStartAt()) ? -1 : 0;
            }
        });

        // Itera na lista de batidas do som
        for (int i = 0; i < song.getBeats().size(); i++) {
            Beat currentBeat = song.getBeats().get(i);
            playNote(currentBeat.getNote(), currentBeat.getDuration());
//            // Espera a duração da batida atual para seguir adiante
//            try {
//                Thread.sleep((long) (currentBeat.getDuration() * 1000));
//                stopNote();
//            } catch (InterruptedException ex) {
//                Log.d("BEAT_DURATION", "Error in " + i);
//            }
        }
    }

    public void playNote(Note note, float duration) {
        if (note == Note.TUM) {
            // Construct a note ON message for the middle C at maximum velocity on channel 1:
            event = new byte[3];
            event[0] = (byte) 153;//(0x90 | 0x00); // 0x90 = note On, 0x00 = channel 1
            event[1] = (byte) 51; // 0x3C = middle C
            event[2] = (byte) 127; // 0x7F = the maximum velocity (127)
        } else if (note == Note.PA) {

            event = new byte[3];
            event[0] = (byte) 153;
            event[1] = (byte) 52;
            event[2] = (byte) 127;
        }

        // Send the MIDI event to the synthesizer.
        midiDriver.write(event);

        // Espera a duração da batida atual para seguir adiante
        try {
            Thread.sleep((long) (duration * 1000));
            stopNote();
        } catch (InterruptedException ex) {
            Log.d("BEAT_DURATION", "Error in " + note);
        }

//        // 144-159
//        for(int k = 153; k <= 153; k++)
//        {
//            Log.d("CHANNEL_START", "C" + k + "=================================================");
//            for (int i = 27; i <= 87; i++) {
//                event = new byte[3];
//                event[0] = (byte) k;
//                event[1] = (byte) i;
//                event[2] = (byte) 127;
//                midiDriver.write(event);
//
//                Log.d("NOTE", "" + i);
//                try {
//                    Thread.sleep(2000);
//                    stopNote();
//                } catch (InterruptedException ex) {
//                    Log.d("BEAT_THREAD_SLEEP", "" + i);
//                }
//            }
//            Log.d("CHANNEL_END", "========================================================");
//        }
    }

    public void stopNote() {
        // Construct a note OFF message for the middle C at minimum velocity on channel 1:
        event = new byte[3];
        event[0] = (byte) (0x80 | 0x00);  // 0x80 = note Off, 0x00 = channel 1
        event[1] = (byte) 0x3C;  // 0x3C = middle C
        event[2] = (byte) 0x00;  // 0x00 = the minimum velocity (0)

        // Send the MIDI event to the synthesizer.
        midiDriver.write(event);
    }
}